import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;

    Widget contactMe = Container(
      padding: const EdgeInsets.all(40),
      child: Row(
        children: [
          Expanded(
              child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'WICHAI JITRAGUL',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Text('164 Mitsamphan rd., mueng, Chonburi, Thailand 20130'),
              Text('093-941-8865, wichai.jitragul@gmail.com'),
            ],
          )),
          Image.asset(
            'images/PictureResume.jpg',
            width: 120,
            height: 140,
          )
        ],
      ),
    );

    Widget myInformation = Container(
        padding: EdgeInsets.only(left: 20),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.all(10),
                    child: Text(
                      'MY INFORMATION',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 30),
                    child: Text(
                      '• Date of Birth : September 28,1999',
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 30),
                    child: Text(
                      '• Gender : Male',
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 30),
                    child: Text(
                      '• Nationality : Thai',
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 30),
                    child: Text(
                      '• Military Service : Exempted',
                    ),
                  ),
                ],
              ),
            )
          ],
        ));

    Widget myEducation = Container(
        padding: EdgeInsets.only(left: 20),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.all(10),
                    child: Text(
                      'EDUCATION',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text('2018 - Present'),
                  Container(
                    padding: const EdgeInsets.only(left: 30),
                    child: Text(
                      'CS (Computer Science) : Faclty of Informatics : Bachelor of Science(B.S.) - in progress',
                    ),
                  ),
                  Text('2016 - 2018'),
                  Container(
                    padding: const EdgeInsets.only(left: 30),
                    child: Text(
                        'High School : High School Certificate : Saensuk School'),
                  ),
                ],
              ),
            )
          ],
        ));

    Widget textMyskill = Container(
      child: Row(
        children: [
          Container(
            padding: const EdgeInsets.only(top: 10, left: 30, bottom: 10),
            child: Text(
              'MY SKILL',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );

    Widget mySkill = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Image.asset(
            'images/html5.png',
            width: 60,
            height: 80,
          ),
          Image.asset(
            'images/javascript.png',
            width: 60,
            height: 80,
          ),
          Image.asset(
            'images/lucidChart.png',
            width: 40,
            height: 60,
          ),
          Image.asset(
            'images/Vuejs.jpeg',
            width: 80,
            height: 100,
          ),
        ],
      ),
    );

    Widget myHobby = Container(
        padding: EdgeInsets.only(left: 20),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.all(10),
                    child: Text(
                      'MY HOBBY',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 30),
                    child: Text(
                      '• Play VDOGame',
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 30),
                    child: Text(
                      '• Cook',
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 30),
                    child: Text(
                      '• Watch Movie',
                    ),
                  ),
                ],
              ),
            )
          ],
        ));
    return MaterialApp(
      title: 'Resume',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Resume'),
        ),
        body: ListView(
          children: [
            contactMe,
            myInformation,
            myEducation,
            textMyskill,
            mySkill,
            myHobby
          ],
        ),
      ),
    );
  }
}
